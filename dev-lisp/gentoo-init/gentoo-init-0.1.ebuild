# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-lisp/cl-zip/cl-zip-20050405.ebuild,v 1.2 2005/11/01 23:53:31 vapier Exp $

DESCRIPTION=""
HOMEPAGE=""
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~sparc ~x86"
IUSE=""

S=${WORKDIR}

src_install() {
	insinto /etc
	doins ${FILESDIR}/gentoo-init.lisp
}
